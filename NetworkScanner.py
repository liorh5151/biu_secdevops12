
# In this file we will scan the given IP address and its range

#Importing Scapy
import scapy.all as scapy
from scapy import *
#import regular expressions
import re
#import aioarping for using ARP protocol
# import aioarping
import arp

#Intro
print("***********************************")
print("*_░N░e░t░w░o░r░k░_░S░c░a░n░n░e░r░_*")
print("***********************************")

#How to recognize IPV4 IP addresses
ip_addr_range_pattern = re.compile("[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}")

# get the ip range to arp - know what that is
while True:
    target_ip = input("\nPlease enter the IP address and the range you're looking for (Exmaple: 192.168.15.10/8")
    if ip_addr_range_pattern.search(target_ip):
        print("{} is indeed in the IP addresses range.").format(target_ip)
    else:
        print("{} is not in the IP addresses range. try again next time.").format(target_ip)
        break

# Here we'll ARP (network discovery protocol) to list the results and discover all the devices in the network
arp_result = arp.ARP_REQUEST(target_ip) > NetworkScanning.txt
# Another way to ARP the chosen IP Address
#arp_result = aioarping.aioarping(target_ip)


# This is my 'Tic Tac Toe' game as my project [BIU SecDevOpes 12].

from function import draw_board, whos_turn, winning
from function import terms
import os

terms()

print("This is the first round, good luck!")
table = {1 : '1', 2 : '2', 3 : '3', 4 : '4', 5 : '5',
         6 : '6', 7 : '7', 8 : '8', 9 : '9'}


# A 'While' loop in order to update the table


play_time = True
complete = False
turn = 0
already_taken = -1

while play_time:
    # Refreshes the screen every turn
    os.system('cls' if os.name == 'nt' else 'clear')
    # To start the game
    draw_board(table)
    # Making sure there are no double turns
    if already_taken == turn:
        print("Sorry, this spot is already selected, Please choose another one.")
    already_taken = turn
    # Asking the turn's player to choose their spot or stop the game
    print("Player " + str((turn % 2) + 1 ) + "'s turn: pick a spot or press q to quit the game")
    # Asking the first person for their turn
    choice = input("Please choose a number:")
    if choice == 'q':
        play_time = False
    elif str.isdigit(choice) and int(choice) in table:
        # To unable only "X", or "O" answers:
        if not table[int(choice)] in {"X", "O"}:
            # Every round the number of the turns will raise in 1 so we can know when to operate
            turn = turn + 1
            # To keep playing when the turn is ours
            table[int(choice)] = whos_turn(turn)

    # To check is someone won the game
    if winning(table): play_time, complete = False, True

# The results of the game
os.system('cls' if os.name == 'nt' else 'clear')
draw_board(table)
if complete:
    if whos_turn(turn) == 'X':
        print("Player number one won the game!")
    else:
        print("Player number two has won the game!")
else:
    print("No winner this time! Try again.")


print("Thank you for playing! Hope to see you soon :)")

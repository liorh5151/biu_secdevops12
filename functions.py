# This file is the functions file for the 'Tic Tac Toe' game.

# Asking the player is they wanna play to view the terms and conditions
def terms():
    answer = input("Hello dear player, would you like to play TicTacToe? Y/N")
    if answer == "Y":
        print("Here are the terms of the game:\n"
              "Tic-tac-toe is a paper-and-pencil game for two players,\n"
           "who take turns marking the spaces in a three-by-three grid with X or O.\n"
        "The player who succeeds in placing three of their marks in a horizontal, vertical, or diagonal row is the winner.\n"
              "If you wish to stop in the middle of the game, press 'q'. HAVE FUN!")
    elif answer == "N":
        print("See you next time!")
    else:
        print("Sorry, this is an invalid answer, try again.")
        terms()

# A function for the game's table
def draw_board(table):
    board = (f"|{table[1]}|{table[2]}|{table[3]}|\n"
             f"|{table[4]}|{table[5]}|{table[6]}|\n"
             f"|{table[7]}|{table[8]}|{table[9]}|")
    print(board)


# A function to check who's turn it is
def whos_turn(turn):
    if turn % 2 == 0:
        return 'O'
    else:
        return 'X'

def winning(table):
    # Winning in a row
    if (table[1]) == (table[2]) == (table[3]) \
       or (table[4]) == (table[5]) == (table[6]) \
       or (table[7]) == (table[8]) == (table[9]):
        return True
    elif (table[1]) == (table[4]) == (table[7]) \
       or (table[2]) == (table[5]) == (table[8]) \
       or (table[3]) == (table[6]) == (table[9]):
        return True
    elif (table[1]) == (table[5]) == (table[9]) \
       or (table[3]) == (table[5]) == (table[7]):
        return True
    else:
    return False



# [
#         final_answer = input("Do you wish to continue? Y/N")
#         if final_answer == 'Y':
#             draw_board(--------------------------------- enter game-----------------)
#         elif answer == "N":
#             print("See you next time!")
#             terms()
#         else:
#             print("Sorry, this is an invalid answer, try again.")
#             terms()
#
#
#
# ]

